package com.btrco.example.sbet.landon.roomwebapp.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "EMPLOYEE")
public class StaffMember {

    @Id
    @Column(name = "EMPLOYEE_ID")
    private String employeeId;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Enumerated(EnumType.STRING)
    @Column(name = "POSITION")
    private Position position;

    public StaffMember() {
        this.employeeId = UUID.randomUUID().toString();
    }
}
