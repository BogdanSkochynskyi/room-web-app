package com.btrco.example.sbet.landon.roomwebapp.service;

import com.btrco.example.sbet.landon.roomwebapp.data.RoomRepository;
import com.btrco.example.sbet.landon.roomwebapp.models.Room;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;

    public List<Room> getAllRooms(){
        return roomRepository.findAll();
    }

    public Room getById(long id) {
        return roomRepository.findById(id).get();
    }
}
