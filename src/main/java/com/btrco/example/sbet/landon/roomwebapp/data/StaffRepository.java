package com.btrco.example.sbet.landon.roomwebapp.data;

import com.btrco.example.sbet.landon.roomwebapp.models.StaffMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffRepository extends JpaRepository<StaffMember, String> {
}
