package com.btrco.example.sbet.landon.roomwebapp.service;

import com.btrco.example.sbet.landon.roomwebapp.data.StaffRepository;
import com.btrco.example.sbet.landon.roomwebapp.models.StaffMember;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StaffService {

    private final StaffRepository staffRepository;

    public List<StaffMember> getAllStaff() {
        return staffRepository.findAll();
    }
}
