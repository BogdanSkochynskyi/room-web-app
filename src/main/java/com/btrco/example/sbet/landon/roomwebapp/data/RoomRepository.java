package com.btrco.example.sbet.landon.roomwebapp.data;

import com.btrco.example.sbet.landon.roomwebapp.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
}
