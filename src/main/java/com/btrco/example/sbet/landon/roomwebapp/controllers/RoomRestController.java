package com.btrco.example.sbet.landon.roomwebapp.controllers;


import com.btrco.example.sbet.landon.roomwebapp.models.Room;
import com.btrco.example.sbet.landon.roomwebapp.service.RoomService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/rooms")
@AllArgsConstructor
public class RoomRestController {

    private final RoomService roomService;

    @GetMapping
    public List<Room> getAllRooms() {
        return roomService.getAllRooms();
    }
}
